#ifndef BLOCK_OUTPUT_H
#define BLOCK_OUTPUT_H

#include "block.h"

class Block_Output : public Block
{
public:
    Block_Output(QVector2D _pos, int _n);
    QString pin;
    int n;
    int type; // 0: DIGITAL | 1: ANALOG | 2: SERVO
    QString include();
    QString define();
    QString setup();
    QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_OUTPUT_H
