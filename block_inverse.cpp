#include "block_inverse.h"

Block_Inverse::Block_Inverse(QVector2D _pos)
{
    this->position = _pos;
    this->nodesIn = 1;
    this->nodesOut = 1;
    this->imagePath = QDir::currentPath() + "/blocks/Math/inverse.svg";
    this->generateSVG();
    this->size.setX(100);
    this->size.setY(100);
}

QString Block_Inverse::loop(QStringList argsIn, QStringList argsOut)
{
    return "\tfloat " + argsOut.at(0) + " = (float)(1/" + argsIn.at(0) + ");\n";
}
