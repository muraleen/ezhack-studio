#include "block_sum.h"

Block_Sum::Block_Sum(QVector2D _pos)
{
    this->position = _pos;
    this->signList = "++";
    this->nodesIn = 2;
    this->nodesOut = 1;
    this->imagePath = QDir::currentPath() + "/blocks/Math/sum.svg";
    this->generateSVG();
    this->size.setX(100);
    this->size.setY(200);
}

QString Block_Sum::loop(QStringList argsIn, QStringList argsOut)
{
    QString output = "\tfloat " + argsOut.at(0) + " = (float)("; // Cast to float
    for(int i=0; i<this->nodesIn; i++)
    {
        if(this->signList.mid(i,1) == "+")
        {
            if(i != 0)
                output += " + " + argsIn.at(i);
            else
                output += argsIn.at(i);
        } else // -
            if(i != 0)
                output += " - " + argsIn.at(i);
            else
                output += "- " + argsIn.at(i);
    }
    return output + ");\n";
}
