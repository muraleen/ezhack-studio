#ifndef EZHACKSTUDIO_H
#define EZHACKSTUDIO_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "arduinosyntax.h"
#include "codeconverter.h"

namespace Ui {
class ezHackStudio;
}

class ezHackStudio : public QMainWindow
{
    Q_OBJECT

public:
    explicit ezHackStudio(QWidget *parent = 0);
    ~ezHackStudio();

private slots:
    void on_actionConvert_triggered();

private:
    Ui::ezHackStudio *ui;
    ArduinoSyntax *highlighter;
    CodeConverter *cc;
};

#endif // EZHACKSTUDIO_H
