#ifndef BLOCK_INVERSE_H
#define BLOCK_INVERSE_H

#include "block.h"

class Block_Inverse : public Block
{
public:
    Block_Inverse(QVector2D _pos);
    virtual QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_INVERSE_H
