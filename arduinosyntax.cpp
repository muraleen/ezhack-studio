#include "arduinosyntax.h"
#include <QDir>
#include <QTextStream>
#include <QDebug>

ArduinoSyntax::ArduinoSyntax(QTextDocument *parent) : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    // ARDUINO SYNTAX HIGHLIGHTING RULES

    QStringList keywordPatterns;

    // Function format (something followed by ())
    functionFormat.setForeground(Qt::darkRed);
    functionFormat.setFontWeight(QFont::Bold);
    rule.pattern = QRegExp("\\b[A-Za-z0-9_]+(?=\\()");
    rule.format = functionFormat;
    highlightingRules.append(rule);

    // C++ Keywords
    keywordFormat.setForeground(Qt::darkYellow);
    // keywordFormat.setFontWeight(QFont::Bold);
    QFile file1(QDir::currentPath() + "/syntax/keywords.txt");
    file1.open(QIODevice::ReadOnly);
    QTextStream in1(&file1);
    while(!in1.atEnd()) {
        keywordPatterns << QString("\\b%1\\b").arg(in1.readLine());
    }
    file1.close();
    foreach (const QString &pattern, keywordPatterns) {
         rule.pattern = QRegExp(pattern);
         rule.format = keywordFormat;
         highlightingRules.append(rule);
    }

    // Arduino Constants
    constantFormat.setForeground(Qt::darkBlue);
    QFile file2(QDir::currentPath() + "/syntax/constants.txt");
    file2.open(QIODevice::ReadOnly);
    QTextStream in2(&file2);
    keywordPatterns.clear();
    while(!in2.atEnd()) {
        keywordPatterns << QString("\\b%1\\b").arg(in2.readLine());
    }
    file2.close();
    foreach (const QString &pattern, keywordPatterns) {
         rule.pattern = QRegExp(pattern);
         rule.format = constantFormat;
         highlightingRules.append(rule);
    }

    // Arduino Functions
    arduinoFormat.setForeground(Qt::darkRed);
    QFile file3(QDir::currentPath() + "/syntax/functions.txt");
    file3.open(QIODevice::ReadOnly);
    QTextStream in3(&file3);
    keywordPatterns.clear();
    while(!in3.atEnd()) {
        keywordPatterns << QString("\\b%1\\b").arg(in3.readLine());
    }
    file3.close();
    foreach (const QString &pattern, keywordPatterns) {
         rule.pattern = QRegExp(pattern);
         rule.format = arduinoFormat;
         highlightingRules.append(rule);
    }

    // Operators
    operatorFormat.setForeground(Qt::darkMagenta);
    QFile file4(QDir::currentPath() + "/syntax/operators.txt");
    file4.open(QIODevice::ReadOnly);
    QTextStream in4(&file4);
    keywordPatterns.clear();
    while(!in4.atEnd()) {
        keywordPatterns << QString("%1").arg(in4.readLine());
    }
    file4.close();
    foreach (const QString &pattern, keywordPatterns) {
         rule.pattern = QRegExp(pattern);
         rule.format = operatorFormat;
         highlightingRules.append(rule);
    }

    // Hash Tag (#)
    includeFormat.setForeground(Qt::darkBlue);
    rule.pattern = QRegExp("#[^\n]*");
    rule.format = includeFormat;
    highlightingRules.append(rule);

    // Block Variable (blockX_Y)
    blockFormat.setForeground(QColor(120,120,120));
    rule.pattern = QRegExp("block[^\s]");
    rule.format = blockFormat;
    highlightingRules.append(rule);

    // Strings in (single and double) quotations
    quotationFormat.setForeground(QColor(60,115,180));
    rule.pattern = QRegExp("\".*\"");
    rule.format = quotationFormat;
    highlightingRules.append(rule);
    rule.pattern = QRegExp("\'.*\'");
    highlightingRules.append(rule);

    // Include (<>)
    includeFormat.setForeground(QColor(90,170,125));
    includeFormat.setFontWeight(QFont::Bold);
    rule.pattern = QRegExp("<.*>");
    rule.format = includeFormat;
    highlightingRules.append(rule);

    // Single line comment
    singleLineCommentFormat.setForeground(QColor(30,140,70));
    rule.pattern = QRegExp("//[^\n]*");
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    // Multiple lines comment
    multiLineCommentFormat.setForeground(QColor(30,140,70));

    commentStartExpression = QRegExp("/\\*");
    commentEndExpression = QRegExp("\\*/");
}

void ArduinoSyntax::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = expression.indexIn(text, index + length);
        }
    }
    setCurrentBlockState(0);
     int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = commentStartExpression.indexIn(text);
     while (startIndex >= 0) {
        int endIndex = commentEndExpression.indexIn(text, startIndex);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex
                            + commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = commentStartExpression.indexIn(text, startIndex + commentLength);
    }
}
