#include "block_output.h"

Block_Output::Block_Output(QVector2D _pos, int _n)
{
    this->position = _pos;
    this->n = _n;
    this->nodesIn = 1; // Output value variable
    this->pin = "0"; // PIN 0 by default
    this->type = 1; // DIGITAL by default
    this->imagePath = QDir::currentPath() + "/blocks/output.svg";
    this->generateSVG();
    this->size.setX(120);
    this->size.setY(120);
}

QString Block_Output::include()
{
    if(this->type == 2) // SERVO
        return "#include <Servo.h>\n";
    else
        return "";
}

QString Block_Output::define()
{
    if(this->type == 2) // SERVO
        return "Servo servo" + QString::number(this->n) + ";\n";
    else
        return "";
}

QString Block_Output::setup()
{
    return "\tpinMode(" + this->pin + ", OUTPUT);\n";
}

QString Block_Output::loop(QStringList argsIn, QStringList argsOut)
{
    switch(this->type)
    {
    case 0: // DIGITAL
        return "\tdigitalWrite(" + this->pin + ", " + argsIn.at(0) + ");\n";
        break;
    case 1: // ANALOG
        return "\tanalogWrite(" + this->pin + ", " + argsIn.at(0) + ");\n";
        break;
    case 2: // PULSEIN
        return "\tservo_block" + QString::number(this->n) + ".write(" + argsIn.at(0) + ");\n";
        break;
    default:
        return "";
    }
}
