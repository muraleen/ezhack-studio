#include "block_input.h"

Block_Input::Block_Input(QVector2D _pos)
{
    this->position = _pos;
    this->nodesOut = 1; // Input value stored in...
    this->pin = "A0"; // PIN A0 by default
    this->type = 1; // ANALOG by default
    this->pulseIn_edge = 1; // HIGH by default
    this->pulseIn_timeOut = 0; // USE DEFAULT by default
    this->imagePath = QDir::currentPath() + "/blocks/input.svg";
    this->generateSVG();
    this->size.setX(120);
    this->size.setY(60);
}

QString Block_Input::setup()
{
    return "\tpinMode(" + this->pin + ", INPUT);\n";
}

QString Block_Input::loop(QStringList argsIn, QStringList argsOut)
{
    QString inputFn = "\tint " + argsOut.at(0) + " = ";
    switch(this->type)
    {
    case 0: // DIGITAL
        inputFn += "digitalRead(" + this->pin + ");\n";
        break;
    case 1: // ANALOG
        inputFn += "analogRead(" + this->pin + ");\n";
        break;
    case 2: // PULSEIN
        if(this->pulseIn_timeOut > 0)
            inputFn += "pulseIn(" + this->pin + ", " + this->pulseIn_edge + ", " + this->pulseIn_timeOut + ");\n";
        else
            inputFn += "pulseIn(" + this->pin + ", " + this->pulseIn_edge + ");\n";
        break;
    }
    return inputFn;
}
