#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <QString>

class Connector
{
public:
    Connector(int _bIn, int _nIn, int _bOut, int _nOut);
    QString varName;
    // Contains only block and node IDs for faster access
    int blockIn;
    int blockOut;
    int nodeIn;
    int nodeOut;
    void rmBlock(int _block);
};

#endif // CONNECTOR_H
