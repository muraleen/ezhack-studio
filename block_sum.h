#ifndef BLOCK_SUM_H
#define BLOCK_SUM_H

#include "block.h"

class Block_Sum : public Block
{
public:
    Block_Sum(QVector2D _pos);
    QString signList;
    virtual QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_SUM_H
