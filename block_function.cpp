#include "block_function.h"

Block_Function::Block_Function(QVector2D _pos)
{
    this->position = _pos;
    this->nodesIn = 1;
    this->nodesOut = 1;
    this->function = "0";
    this->imagePath = QDir::currentPath() + "/blocks/Math/function.svg";
    this->generateSVG();
    this->size.setX(100);
    this->size.setY(00);
}

QString Block_Function::loop(QStringList argsIn, QStringList argsOut)
{
    return "\t" + argsOut.at(0) + " = " + this->function + ";\n";
}

