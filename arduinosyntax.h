#ifndef ARDUINOSYNTAX_H
#define ARDUINOSYNTAX_H

#include <QSyntaxHighlighter>

class ArduinoSyntax : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    ArduinoSyntax(QTextDocument *parent = 0);

protected:
    void highlightBlock(const QString &text);

private:
    struct HighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    QRegExp commentStartExpression;
    QRegExp commentEndExpression;

    QTextCharFormat keywordFormat;
    QTextCharFormat constantFormat;
    QTextCharFormat arduinoFormat;
    QTextCharFormat includeFormat;
    QTextCharFormat hashTagFormat;
    QTextCharFormat singleLineCommentFormat;
    QTextCharFormat multiLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat functionFormat;
    QTextCharFormat operatorFormat;
    QTextCharFormat blockFormat;
};

#endif // ARDUINOSYNTAX_H
