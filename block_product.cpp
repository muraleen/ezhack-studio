#include "block_product.h"

Block_Product::Block_Product(QVector2D _pos)
{
    this->position = _pos;
    this->nodesIn = 2;
    this->nodesOut = 1;
    this->imagePath = QDir::currentPath() + "/blocks/Math/product.svg";
    this->generateSVG();
    this->size.setX(100);
    this->size.setY(200);
}

QString Block_Product::loop(QStringList argsIn, QStringList argsOut)
{
    QString output = "\tfloat " + argsOut.at(0) + " = (float)(" + argsIn.at(0); // Cast to float
    for(int i=1; i<this->nodesIn; i++)
    {
        output += " * " + argsIn.at(i);
    }
    return output + ");\n";
}
