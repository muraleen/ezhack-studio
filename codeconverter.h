#ifndef CODECONVERTER_H
#define CODECONVERTER_H

#include <QList>
#include "block.h"
#include "connector.h"

class CodeConverter
{
public:
    CodeConverter();
    QString convert(QList<Block*> blocks, QList<Connector*> connectors);
};

#endif // CODECONVERTER_H
