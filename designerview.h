#ifndef DESIGNERVIEW_H
#define DESIGNERVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QTransform>
#include <QVector2D>

class DesignerView : public QGraphicsView
{
    Q_OBJECT

public:
    DesignerView(QWidget* parent = NULL);
    void drawBlocks();

protected:
    virtual void wheelEvent(QWheelEvent* event);

private:
    QGraphicsScene *designer;
    void drawGrid();
    QTransform moveBlock(QVector2D _pos);
};

#endif // DESIGNERVIEW_H
