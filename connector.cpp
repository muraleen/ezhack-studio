#include "connector.h"

Connector::Connector(int _bIn, int _nIn, int _bOut, int _nOut)
{
    // Right now, I'm basing the connector name on the connector ins (so, block output nodes)
    this->varName = "block" + QString::number(_bIn) + "_" + QString::number(_nIn);
    this->blockIn = _bIn;
    this->blockOut = _bOut;
    this->nodeIn = _nIn;
    this->nodeOut = _nOut;
}

void Connector::rmBlock(int _block)
{
    if(_block < blockIn)
        blockIn --;

    if(_block < blockOut)
        blockOut --;
}
