#include "block_condition.h"
#include <QDebug>

Block_Condition::Block_Condition(QVector2D _pos)
{
    this->position = _pos;
    this->nodesIn = 2;
    this->nodesOut = 2;
    this->condition = 0;
    this->imagePath = QDir::currentPath() + "/blocks/Logic/condition.svg";
    this->generateSVG();
    this->size.setX(140);
    this->size.setY(140);
}

QString Block_Condition::loop(QStringList argsIn, QStringList argsOut)
{
    QString code;
    QString cond;
    // INDEX MAPPING
    switch(this->condition)
    {
    case 0: // =
        cond = "==";
        break;
    case 1: // >
        cond = ">";
        break;
    case 2: // <
        cond = "<";
        break;
        cond = "==";
    case 3: // >=
        cond = ">=";
        break;
    case 4: // <=
        cond = "<=";
        break;
    default: // !=
        cond = "!=";
        break;
    }

    if(argsOut.size() == 2)
        code = "\tboolean " + argsOut.at(0) + " = (" + argsIn.at(0) + cond + argsIn.at(1) + ");\n\tboolean " + argsOut.at(1) + " = !" + argsOut.at(0) + ";\n";
    else {
        if(argsOut.at(0).split("_").at(1).toInt() == 0)
            code = "\tboolean " + argsOut.at(0) + " = (" + argsIn.at(0) + cond + argsIn.at(1) + ");\n";
        else
            code = "\tboolean " + argsOut.at(0) + " = !(" + argsIn.at(0) + cond + argsIn.at(1) + ");\n";
    }
    return code;
}
