# ezHack Studio 1.0 - README #

### What is the ezHack Studio? ###

* The ezHack Studio is a very easy-to-use block-model based integrated development environment for arduino-compatible micro-controllers. This program allows you to program your uHack (or any other arduino-compatible board) using simple drag-and-drop blocks and connection nodes. Check out our [webpage](http://silverwingaero.com/ezHackStudio) for more information on the program.

Check out the [open source project repository](https://bitbucket.org/tutorials/markdowndemo) to stay up to date with the development.