#ifndef GLOBAL_H
#define GLOBAL_H

// GLOBAL INCLUDES
#include "block.h"
#include "connector.h"
#include <QList>

#include "block_input.h"
#include "block_output.h"
#include "block_function.h"
#include "block_sum.h"
#include "block_product.h"
#include "block_inverse.h"
#include "block_constant.h"
#include "block_condition.h"

// GLOBAL VARIABLES
extern QList<Block*> blocks;
extern QList<Connector*> connectors;

#endif // GLOBAL_H
