#include "block_constant.h"

Block_Constant::Block_Constant(QVector2D _pos)
{
    this->position = _pos;
    this->k = 127;
    this->nodesIn = 2;
    this->nodesOut = 1;
    this->imagePath = QDir::currentPath() + "/blocks/constant.svg";
    this->generateSVG();
    this->size.setX(120);
    this->size.setY(60);
}

QString Block_Constant::loop(QStringList argsIn, QStringList argsOut)
{
    return "\tfloat " + argsOut.at(0) + " = " + QString::number(this->k) + ";\n";
}
