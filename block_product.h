#ifndef BLOCK_PRODUCT_H
#define BLOCK_PRODUCT_H

#include "block.h"

class Block_Product : public Block
{
public:
    Block_Product(QVector2D _pos);
    virtual QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_PRODUCT_H
