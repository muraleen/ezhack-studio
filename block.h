#ifndef BLOCK_H
#define BLOCK_H

#include <QList>
#include <QStringList>
#include <QVector2D>
#include <QDir>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsSvgItem>
#include <QTransform>
// #include "node.h"

// BLOCK BASE CLASS

class Block
{
public:
    Block();
    QVector2D position;
    QVector2D size;
    QString name;
    QGraphicsSvgItem *svg;
    QString imagePath;
    // Code Conversion (virtual) Functions - need to be re-defined by the derived class
    virtual QString include()
        { return ""; }
    virtual QString define()
        { return ""; }
    virtual QString setup()
        { return ""; }
    virtual QString loop(QStringList argsIn, QStringList argsOut)
        { return ""; }
    virtual void generateSVG() {
        this->svg = new QGraphicsSvgItem(this->imagePath);
    }
    virtual void render(QGraphicsScene *designer) {
        QGraphicsSvgItem *item = this->svg;
        item->setTransform(QTransform(1,0,0,1,position.x(),position.y()));
        designer->addItem(item);
    }

    int nodesIn;
    int nodesOut;
};

#endif // BLOCK_H
