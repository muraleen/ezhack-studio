#ifndef BLOCK_FUNCTION_H
#define BLOCK_FUNCTION_H

#include "block.h"

class Block_Function : public Block
{
public:
    Block_Function(QVector2D _pos);
    QString function;
    virtual QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_FUNCTION_H
