#ifndef BLOCK_CONDITION_H
#define BLOCK_CONDITION_H

#include "block.h"

class Block_Condition : public Block
{
public:
    Block_Condition(QVector2D _pos);
    int condition; // See switch-case statement in block_condition.cpp for index mapping
    virtual QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_CONDITION_H
