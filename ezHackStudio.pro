#-------------------------------------------------
#
# Project created by QtCreator 2014-11-02T13:47:29
#
#-------------------------------------------------

QT       += core gui svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ezHackStudio
TEMPLATE = app


SOURCES += main.cpp\
        ezhackstudio.cpp \
    arduinosyntax.cpp \
    codeeditor.cpp \
    designerview.cpp \
    block.cpp \
    node.cpp \
    connector.cpp \
    block_input.cpp \
    block_output.cpp \
    codeconverter.cpp \
    global.cpp \
    block_function.cpp \
    block_sum.cpp \
    block_product.cpp \
    block_inverse.cpp \
    block_constant.cpp \
    block_condition.cpp

HEADERS  += ezhackstudio.h \
    arduinosyntax.h \
    codeeditor.h \
    designerview.h \
    block.h \
    node.h \
    connector.h \
    block_input.h \
    block_output.h \
    codeconverter.h \
    global.h \
    block_function.h \
    block_sum.h \
    block_product.h \
    block_inverse.h \
    block_constant.h \
    block_condition.h

FORMS    += ezhackstudio.ui

OTHER_FILES +=

RESOURCES += \
    resources.qrc
