#include "codeeditor.h"
#include <QtGui>

CodeEditor::CodeEditor(QWidget *parent) : QPlainTextEdit(parent)
{
    this->insertPlainText("void setup() {\n\t// Put your setup code here, to run once:\n\t\n}\n\nvoid loop() {\n\t// Put your main code here, to run repeatedly:\n\t\n}");

    lineNumberArea = new LineNumberArea(this);

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));

    updateLineNumberAreaWidth(0);
    highlightCurrentLine();
}

void CodeEditor::keyPressEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_Return)
    {
        QTextCursor _cursor(this->textCursor());
        int cursorPos = _cursor.position(); // Save position to return here later

        // Indent cursor to last indent position
        _cursor.movePosition(QTextCursor::StartOfLine);
        int tabs = 0;
        while(this->document()->characterAt(_cursor.position()) == '\t') // Count TABS
        {
            _cursor.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, 1);
            tabs++;
        }
        _cursor.movePosition(QTextCursor::Start);
        _cursor.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, cursorPos);
        this->setTextCursor(_cursor);
        this->insertPlainText("\n");
        for(int i=0; i<tabs; i++)
        {
            this->insertPlainText("\t");
        }

        _cursor.movePosition(QTextCursor::StartOfLine);
        // Check if and end brace is required
        QChar lastChar = this->document()->characterAt(_cursor.position()-2);
        if(lastChar == '{')
        {
            this->insertPlainText("\t\n");
            for(int i=0; i<tabs; i++)
            {
                this->insertPlainText("\t");
            }
            this->insertPlainText("}");
            if(tabs > 0)
                _cursor.movePosition(QTextCursor::EndOfLine);
            else
                _cursor.movePosition(QTextCursor::Left, QTextCursor::MoveAnchor, 2);

            this->setTextCursor(_cursor);
        }
    } else {
        QPlainTextEdit::keyPressEvent(e);
    }
}

int CodeEditor::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while(max >= 10)
    {
        max /= 10;
        ++digits;
    }

    int space = 3 + fontMetrics().width(QLatin1Char('9'))*digits;

    return space;
}

void CodeEditor::updateLineNumberAreaWidth(int newBlockCount)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if(dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());
    if(rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void CodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);
    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;
    if(!isReadOnly()) {
        QTextEdit::ExtraSelection selection;
        QColor lineColor = QColor(245,245,245);
        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}

 void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.fillRect(event->rect(), QColor(240,240,240));
    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();
    while(block.isValid() && top <= event->rect().bottom()) {
        if(block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(QColor(120,120,120));
            painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(),
                             Qt::AlignRight, number);
        }
        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}
