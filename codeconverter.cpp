#include "codeconverter.h"
#include <QDebug>

CodeConverter::CodeConverter()
{
    // CONSTRUCTOR - Not REQ.
}

QString CodeConverter::convert(QList<Block *> blocks, QList<Connector *> connectors)
{
    QString code = "";

    if((blocks.size() > 0) && (connectors.size() > 0)) // Only write the code of there are blocks and connectors - this is to prevent an error in the algorithm
    {

        // Code Conversion algorithm
        /* NOTES:
         * Connectors are used to define order of items to be converted and equating variables. This does not create optimal code for certain blocks and connector
         * situations, and so needs to be optimized. Some things that can be done are checking top to bottom for variables and only using the required variables.
         *
         * Currently, my plan is to start with the input block of the first connector in the list, and give it a "sequence score" of 0. From there, check all the
         * blocks connected to the input nodes of the current block and give it a score of the current block's score - 1. Then, give the blocks connected to the
         * output nodes of the current block a sequence score of the current block's score + 1. Then, keep a list of the blocks scores were assigned to and "visit"
         * each of these blocks until there is none left.
         */

        // STEP 1: INCLUDE HEADERS
        foreach(Block *block, blocks) {
            code += block->include();
        }

        code += "\n";

        // STEP 2: DEFINE VARIABLES
        foreach(Block *block, blocks) {
            code += block->define();
        }

        // int tabs = 0;

        code += "\n// Setup Function\nvoid setup() {\n";

        // STEP 3: SETUP FUNCTION
        //      Currently, the setup function will not support doing things in a certain sequence (as I didn't think that would be necessary at the time I designed
        //      the algorithm. Usually, it's not required but in some cases, it can be very helpful. That definitely needs to be supported soon.
        foreach(Block *block, blocks) {
            code += block->setup();
        }

        code += "}\n\n// Main Loop\nvoid loop() {\n";

        // STEP 4: LOOP FUNCTION
        QList<int> connectorsLeft;
        for(int i=0; i<connectors.size(); i++)
        {
            connectorsLeft.append(i); // To incorporate multiple non-connected series of functional segments.
        }

        // Give each block a "sequence score"
        QList<int> sequenceScores, checkedBlocks;
        for(int i=0; i<blocks.size(); i++)
        {
            sequenceScores.append(0);
        }

        // Start with the input block of the first defined connector
        QList<int> checkBlocks;
        checkBlocks.append(connectors.at(0)->blockIn);
        while(checkBlocks.size() > 0) // Loop as long as there is something
        {
            int blockIndex = checkBlocks.at(0);

            // Get lists of blocks connecting IN to the current block and OUT of the current block
            QList<int> connectInBlocks, connectOutBlocks;
            for(int i=0; i<connectors.size(); i++)
            {
                if(connectors.at(i)->blockOut == blockIndex) // INPUTS
                    connectInBlocks.append(connectors.at(i)->blockIn);

                if(connectors.at(i)->blockIn == blockIndex) // OUTPUTS
                    connectOutBlocks.append(connectors.at(i)->blockOut);
            }
            // Assign a "sequence score" of the current block's score - 1 to the input blocks and + 1 to the output blocks.
            foreach(int block, connectInBlocks)
            {
                sequenceScores.replace(block,sequenceScores.at(blockIndex) - 1);
                bool checked = false;
                foreach(int i, checkedBlocks)
                    if(i == block)
                        checked = true;

                if(!checked)
                    checkBlocks.append(block);
            }

            foreach(int block, connectOutBlocks)
            {
                sequenceScores.replace(block,sequenceScores.at(blockIndex) + 1);
                bool checked = false;
                foreach(int i, checkedBlocks)
                    if(i == block)
                        checked = true;

                if(!checked)
                    checkBlocks.append(block);
            }

            checkedBlocks.append(blockIndex);
            checkBlocks.removeAt(0);
        }

        QList<int> sortedList;

        // Sort list by sequence score
        for(int i=0; i<sequenceScores.size(); i++)
        {
            int min = 99;
            int index;
            // Find minimum value (best score)
            for(int j=0; j<sequenceScores.size(); j++)
            {
                if(sequenceScores.at(j) <= min)
                {
                    min = sequenceScores.at(j);
                    index = j;
                }

            }
            sortedList.append(index); // Append to sorted list
            sequenceScores.replace(index,100); // Get rid of score so it won't find it again
        }

        for(int b=0; b<sortedList.size(); b++)
        {
            Block *block = blocks.at(sortedList.at(b));
            // Get list of input and output connectors
            QList<Connector*> inCon, outCon;
            foreach(Connector *con, connectors)
            {
                // GET INPUT ARGUMENTS
                if(con->blockOut == sortedList.at(b))
                    inCon.append(con);

                // GET OUTPUT ARGUMENTS
                if(con->blockIn == sortedList.at(b))
                    outCon.append(con);
            }

            // Sort connectors based on node index
            int firstNode;
            int index;
            QList<Connector*> inConSorted, outConSorted;
            for(int j=0; j<inCon.size(); j++)
            {
                firstNode = 99;
                for(int i=0; i<inCon.size(); i++)
                {
                    if(inCon.at(i)->nodeOut <= firstNode)
                    {
                        firstNode = inCon.at(i)->nodeOut;
                        index = i;
                    }
                }
                inConSorted.append(inCon.at(index));
                inCon.at(index)->nodeOut = 100;
            }

            for(int j=0; j<outCon.size(); j++)
            {
                firstNode = 99;
                for(int i=0; i<outCon.size(); i++)
                {
                    if(outCon.at(i)->nodeIn <= firstNode)
                    {
                        firstNode = outCon.at(i)->nodeIn;
                        index = i;
                    }
                }
                outConSorted.append(outCon.at(index));
                outCon.at(index)->nodeIn += 100;
            }

            // Generate input and output argument strings list
            QStringList argsIn, argsOut;
            // INPUT ARGUMENTS
            for(int i=0; i<inConSorted.size(); i++) {
                if(blocks.at(inConSorted.at(i)->blockIn)->nodesOut > 1)
                    argsIn << ("block" + QString::number(inConSorted.at(i)->blockIn) + "_" + QString::number(inConSorted.at(i)->nodeIn-100));
                else
                    argsIn << ("block" + QString::number(inConSorted.at(i)->blockIn));
            }
            // OUTPUT ARGUMENTS
            for(int i=0; i<outConSorted.size(); i++) {
                if(blocks.at(outConSorted.at(i)->blockIn)->nodesOut > 1)
                    argsOut << ("block" + QString::number(outConSorted.at(i)->blockIn) + "_" + QString::number(inConSorted.at(i)->nodeIn-100));
                else
                    argsOut << ("block" + QString::number(outConSorted.at(i)->blockIn));
            }

            code += block->loop(argsIn, argsOut);
        }


        code += "}";

        // tabs = 0;

    }

    return code;
}
