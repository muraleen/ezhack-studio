#ifndef BLOCK_INPUT_H
#define BLOCK_INPUT_H

#include "block.h"

class Block_Input : public Block
{
public:
    Block_Input(QVector2D _pos);
    QString pin;
    int type; // 0: DIGITAL | 1: ANALOG | 2: PULSEIN
    int pulseIn_edge; // 0: FALLING EDGE | 1: RISING EDGE
    int pulseIn_timeOut;
    virtual QString setup();
    virtual QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_INPUT_H
