#ifndef BLOCK_CONSTANT_H
#define BLOCK_CONSTANT_H

#include "block.h"

class Block_Constant: public Block
{
public:
    Block_Constant(QVector2D _pos);
    float k;
    virtual QString loop(QStringList argsIn, QStringList argsOut);
};

#endif // BLOCK_CONSTANT_H
