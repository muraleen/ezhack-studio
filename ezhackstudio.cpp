#include "ezhackstudio.h"
#include "ui_ezhackstudio.h"
#include "global.h"
#include <QGraphicsItem>
#include <QDir>
#include <QDebug>

ezHackStudio::ezHackStudio(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ezHackStudio)
{
    ui->setupUi(this);
    highlighter = new ArduinoSyntax(ui->codeEditor->document());
    QWidget::showMaximized();

    cc = new CodeConverter();

    // TEST

    blocks.append(new Block_Input(QVector2D(0,-100)));
    blocks.append(new Block_Constant(QVector2D(0,100)));
    blocks.append(new Block_Condition(QVector2D(200,-40)));
    blocks.append(new Block_Output(QVector2D(500,-100),blocks.size()));
    // blocks.append(new Block_Output(QVector2D(300,240),blocks.size()));

    connectors.append(new Connector(0,0,2,0));
    connectors.append(new Connector(1,0,2,1));
    // connectors.append(new Connector(2,0,3,0));
    connectors.append(new Connector(2,1,3,0));

    ui->designer->drawBlocks();
}

ezHackStudio::~ezHackStudio()
{
    delete ui;
}

void ezHackStudio::on_actionConvert_triggered()
{
    ui->codeEditor->clear();
    ui->codeEditor->insertPlainText(this->cc->convert(blocks, connectors));
}
