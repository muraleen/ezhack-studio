#include "designerview.h"
#include "global.h"

#include <QMouseEvent>
#include <QWheelEvent>
#include <QFileDialog>
#include <QGraphicsSvgItem>

DesignerView::DesignerView(QWidget *parent) : QGraphicsView(parent)
{
    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform | QPainter::TextAntialiasing);

    designer = new QGraphicsScene(this);

    QGraphicsRectItem *rect = new QGraphicsRectItem(-100,-100,200,200);
    designer->addItem(rect);

    QGraphicsEllipseItem *ellipse = new QGraphicsEllipseItem(100,-100,80,160);
    designer->addItem(ellipse);

    // QString imagePath = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("PNG (*.png);; JPEG (*.jpg *.jpeg)"));

    QGraphicsSvgItem *item = new QGraphicsSvgItem("/home/narendran/QtWorkspace/ezHackStudio/blocks/System/delay.svg");

    designer->addItem(item);

    int x = 0, y = 0;

    QTransform matrix(1,0,0,1,x,y);
    item->setTransform(matrix);

    this->setScene(designer);
    this->centerOn(0,0);

}

void DesignerView::wheelEvent(QWheelEvent *event)
{
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    // Scale view (that's how zoom seems to work)
    double scaleFactor = 1.15;
    if(event->delta() > 0)
        scale(scaleFactor,scaleFactor);
    else
        scale(1.0/scaleFactor, 1.0/scaleFactor);
}

void DesignerView::drawGrid()
{
    QColor gridLineColor(230,230,230);
    int bounds = 8000;

    // Draw grid line every 10 pixels
    for(int y=-bounds; y<=bounds; y+=20)
    {
        QGraphicsLineItem *gridLine = new QGraphicsLineItem(-bounds,y,bounds,y);
        gridLine->setPen(QPen(gridLineColor));
        designer->addItem(gridLine);
    }
    for(int x=-bounds; x<=bounds; x+=20)
    {
        QGraphicsLineItem *gridLine = new QGraphicsLineItem(x,-bounds,x,bounds);
        gridLine->setPen(QPen(gridLineColor));
        designer->addItem(gridLine);
    }
}

void DesignerView::drawBlocks()
{
    designer->clear();
    this->drawGrid();

    foreach(Connector *con, connectors)
    {
        QVector2D point1(blocks.at(con->blockIn)->position.x() + blocks.at(con->blockIn)->size.x() - 5, blocks.at(con->blockIn)->position.y() + ((blocks.at(con->blockIn)->size.y()/blocks.at(con->blockIn)->nodesOut)*(0.5 + con->nodeIn)));
        QVector2D point2(blocks.at(con->blockOut)->position.x() + 5, blocks.at(con->blockOut)->position.y() + ((blocks.at(con->blockOut)->size.y()/blocks.at(con->blockOut)->nodesIn)*(0.5 + con->nodeOut)));

        int offset = 30;

        if(point2.x() > point1.x() + 2*offset) // ROUTE FORWARDS [x-bar method]
        {

            if(point2.y() == point1.y())
                designer->addItem(new QGraphicsLineItem(point1.x(),point1.y(),point2.x(),point2.y()));
            else {
                int x_bar = (point1.x()+point2.x())/2; // x Mid-point

                designer->addItem(new QGraphicsLineItem(point1.x(),point1.y(),x_bar,point1.y()));
                designer->addItem(new QGraphicsLineItem(x_bar,point1.y(),x_bar,point2.y()));
                designer->addItem(new QGraphicsLineItem(x_bar,point2.y(),point2.x(),point2.y()));
            }



        } else { // ROUTE BACKWARDS [y-bar method]
            int y_bar; // y Mid-point

            if(point2.y() > point1.y())
                y_bar = (blocks.at(con->blockIn)->position.y() + blocks.at(con->blockIn)->size.y() + blocks.at(con->blockOut)->position.y())/2;
            else
                y_bar = (blocks.at(con->blockOut)->position.y() + blocks.at(con->blockOut)->size.y() + blocks.at(con->blockIn)->position.y())/2;

            designer->addItem(new QGraphicsLineItem(point1.x(),point1.y(),point1.x()+offset,point1.y()));
            designer->addItem(new QGraphicsLineItem(point1.x()+offset,point1.y(),point1.x()+offset,y_bar));
            designer->addItem(new QGraphicsLineItem(point1.x()+offset,y_bar,point2.x()-offset,y_bar));
            designer->addItem(new QGraphicsLineItem(point2.x()-offset,y_bar,point2.x()-offset,point2.y()));
            designer->addItem(new QGraphicsLineItem(point2.x()-offset,point2.y(),point2.x(),point2.y()));
        }

        QGraphicsTextItem *var;

        if(blocks.at(con->blockIn)->nodesOut > 1)
            var = new QGraphicsTextItem("block" + QString::number(con->blockIn) + "_" + QString::number(con->nodeIn));
        else
            var = new QGraphicsTextItem("block" + QString::number(con->blockIn));

        if(point1.y() <= point2.y())
            var->setTransform(QTransform(1,0,0,1,point1.x(),point1.y()-22));
        else
            var->setTransform(QTransform(1,0,0,1,point1.x(),point1.y()-4));

        var->setDefaultTextColor(QColor(200,200,200));
        designer->addItem(var);

        // Add Connector arrow at point2
        designer->addItem(new QGraphicsLineItem(point2.x()-10,point2.y()-8,point2.x()-2,point2.y()));
        designer->addItem(new QGraphicsLineItem(point2.x()-10,point2.y()+8,point2.x()-2,point2.y()));

    }

    foreach(Block *block, blocks)
    {
        block->render(designer);
    }
}
